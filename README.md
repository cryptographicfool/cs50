# Backup of Harvard cs50x 2019 Problem Sets.

I will be updating these as and when I go through the problem sets. The repository is a work in progress. (Lecture and notes files are included in each folder for the specific week.)

## week_0 - pset0

I used scratch and made a simple single level dungeon game in scratch, you need to collect potions as well as avoid goblins, slime and flies in order to reach the chest and win the game.

You can play the game [here](https://scratch.mit.edu/projects/317471408/).

## week_1 - pset1
I was introduced to C in week 1 and solved the following problems.

### mario_less.c
A program that recreates half-pyramid from mario game using hashes (#) instead of blocks.

### mario_more.c
A program that creates double half-pyramid from mario game using hashes (#) instead of blocks.

### cash.c
A program to count minimum number of coins needed to give user the required change using greedy algorithm.

### credit.c
A program to check if a credit card number is valid, and whether it is a MasterCard, VISA or an AmEx card.

## week_2 - pset2
I was introduced to arrays, strings and functions in C.

### caesar.c
A program that converts plaintext to ciphertext using a key provided by the user.
